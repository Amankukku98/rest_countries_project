import React, { Component } from "react";
import './layout.css';
import Country from '../Country/country';
import DropDown from "../dropdown/DropDown";
import SearchIcon from '@mui/icons-material/Search';
import * as CountryApi from '../../api';


class Layout extends Component {

    constructor(props) {
        super(props);

        this.state = {
            countries: [],
            isLoading: true,
            searchCountry: '',
            searchRegion: '',
            hasError: false,

        };
    }

    componentDidMount() {
        CountryApi.getCountries()
            .then(countries => {
                this.setState({
                    countries,
                    isLoading: false,
                    hasError: false,
                });
            })
            .catch((err) => {
                this.setState({
                    countries: [],
                    isLoading: false,
                    hasError: "Failed to load countries",
                });
            })
    }

    searchCountry = (event) => {
        this.setState({ searchCountry: event.target.value });
    }

    searchRegion = (event) => {
        this.setState({ searchRegion: event.target.value });
    }

    render() {

        const { countries, searchCountry, searchRegion } = this.state;
        let countryData = countries.filter((country => {
            return searchRegion ? country.region === searchRegion : true;
        }))

        countryData = countryData.filter((country => {
            return searchCountry ? country.name.toLowerCase().includes(searchCountry.toLocaleLowerCase()) : true;
        }))
        // console.log(countryData);

        if (this.state.hasError) {
            return (
                <div>
                    <h1>{this.state.hasError}</h1>
                </div>
            )
        }


        if (this.state.isLoading) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            );
        }


        return (
            <div>
                <div className='nav-container'>
                    <div className="icon">
                        <SearchIcon />
                        <input type="text" placeholder='Search for a country...' className='search' onChange={this.searchCountry} />
                    </div>
                    <select onChange={event => { this.searchRegion(event) }} className="drop-down">
                        {['Filter by Region', 'Africa', 'Americas', 'Asia', 'Europe', 'Oceania', 'Polar', 'Antarctic Ocean', 'Antarctic'].map((el, index) => {
                            return (<DropDown region={el} key={index} />)
                        })}
                    </select>

                </div>

                < div className="main-container">
                    {countryData.map((country) => {
                        return (
                            <div key={country.name} >
                                <Country {...country} key={country.name} />
                            </div>
                        );
                    })}
                </div>
            </div>

        )
    }
}

export default Layout;