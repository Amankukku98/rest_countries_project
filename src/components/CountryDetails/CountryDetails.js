import React, { Component } from 'react'
import * as CountryApi from '../../api';
import './countryDetails.css';
import { Link } from 'react-router-dom';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

class CountryDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryData: '',
            isLoading: true,
            hasError: false,

        }
    }
    async componentDidMount() {
        const { name } = this.props.match.params;
        try {
            const data = await CountryApi.getRegions(name)
            this.setState({
                countryData: data[0],
                isLoading: false,
                hasError: false,
            })
        } catch (err) {
            this.setState({
                countryData: '',
                isLoading: false,
                hasError: "Failed to get data",
            })
        }

    }
    async componentDidUpdate(prevProps) {
        if (prevProps.match.params.name !== this.props.match.params.name) {
            const { name } = this.props.match.params;
            try {
                const data = await CountryApi.getEachCountries(name)
                this.setState({
                    countryData: data,
                    isLoading: false,
                    hasError: false,
                })
            } catch (err) {
                this.setState({
                    countryData: '',
                    isLoading: false,
                    hasError: "Failed to get data",
                })

            }
        }
    }

    render() {
        const { countryData } = this.state;
        if (countryData !== '') {
            console.log(countryData.currencies)
            return (
                <>
                    <Link to="/" className='btn-container'>
                        <ArrowBackIcon /> <button className='btn'>Back</button>
                    </Link>
                    <div className='card-details'>
                        <img src={countryData.flag} alt="" />
                        <div className='one'>
                            <h1 className='name'>{countryData.name}</h1>
                            <p>Native Name:{countryData.nativeName}</p>
                            <p>Population:{countryData.population}</p>
                            <p>Region:{countryData.region}</p>
                            <p>Sub Region:{countryData.subregion}</p>
                            <p>Capital:{countryData.capital}</p>
                            <h2> Border Countries:</h2>
                            {countryData.borders === undefined ? <p>No Borders</p> : countryData.borders.map((item, index) => {
                                return (
                                    <Link key={index} to={item}>
                                        <button className='border-btn' >{item}</button>
                                    </Link>
                                )
                            })}
                        </div>


                        <div className='two'>
                            <p>Top Level Domain:{countryData.topLevelDomain}</p>
                            <p>Currencies:{countryData.currencies[0].name}</p>
                            <p>Languages:{countryData.languages[0].name}</p>
                        </div>
                    </div>
                </>
            )
        }
    }
}



export default CountryDetails;
