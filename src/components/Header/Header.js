import React from 'react';
import './header.css';
import DarkModeIcon from '@mui/icons-material/DarkMode';
// let dark = document.querySelector('#dark');
// dark.addEventListener('click', () => {
//     document.body.style.backgroundColor = "gray";
//     document.querySelector('.main').style.backgroundColor = "#696969";
//     dark.style.color = 'black';
// })
// dark.addEventListener('dblclick', () => {
//     document.body.style.backgroundColor = "white";
//     document.querySelector('.main').style.backgroundColor = "#F8EAED";
//     dark.style.color = 'gray';

// })
const Header = () => {
    return (
        <div className='header-container'>
            <div className="main">
                <h2>Where in the world?</h2>
                <div className="dark-icon">
                    <DarkModeIcon /> <h4 id='dark'>Dark Mode</h4>
                </div>
            </div>
        </div>
    )
}

export default Header;