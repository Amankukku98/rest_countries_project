import React from 'react'
import './country.css';
import { Link } from 'react-router-dom';
import { Component } from "react"

class Country extends Component {
    render() {
        const { flag, name, population, region, capital } = this.props;
        // console.log(name);
        return (
            <div className="country-main">
                <Link to={`/${name}`} className="country-link">
                    <img src={flag} alt="country_flag" />
                    <h2>{name}</h2>
                    <p>Population:{population}</p>
                    <p>Region:{region}</p>
                    <p>Capital:{capital}</p>
                </Link>

            </div>

        )
    }
}

export default Country;
