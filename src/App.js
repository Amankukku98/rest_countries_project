import './App.css';
import React from "react";
import {
  Switch,
  Route,
  BrowserRouter,
} from "react-router-dom";
import CountryDetails from './components/CountryDetails/CountryDetails';
// import EachCountry from "./components/EachCountry/EachCountry";
import Header from './components/Header/Header';
import Layout from './components/Layout/Layout';

function App() {
  return (

    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" component={Layout} />
        <Route exact path="/:name" component={CountryDetails} />
        {/* <Route exact path="/:code" component={EachCountry} /> */}
      </Switch>
    </BrowserRouter>

  );
}

export default App;
